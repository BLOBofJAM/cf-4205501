public class CFTest
{
    public static void main( String[] args )
    {
        while(true)
        {
            try {
              Thread.sleep(1000);
              System.out.println(InvokeHelloProxy.myInterface.sayHello());
            }
            catch(Throwable t) {
              System.out.println("Dynamic Proxy not set yet.");
            }
        }
    }

    public String getString()
    {
      return "Java says hi";
    }

    public void run()
    {
      while(true)
      {
          try {
            Thread.sleep(1000);
            System.out.println(InvokeHelloProxy.myInterface.sayHello());
          }
          catch(Throwable t) {
            System.out.println("Dynamic Proxy not set yet.");
          }
      }
    }
}
