public class InvokeHelloProxy
{
	public static MyInterface myInterface;

	public InvokeHelloProxy(MyInterface x)
	{
		InvokeHelloProxy.myInterface = x;
	}
	public String invokeHello()
	{
		return InvokeHelloProxy.myInterface.sayHello();
	}
}
