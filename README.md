# CF-4205501
To compile Java classes:
javac {file}.java

To get it working:
1. copy the 2 compiled .class files into the {ColdFusion2018}/cfusion/lib directory
2. Copy the cf directory into {ColdFusion2018}/cfusion/wwwroot directory
3. Run CF 2018
4. Access http://{CF hostname}:{port}/cf/Hello.cfm
